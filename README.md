# dotFiles

My Linux configuration files

*Packages:*

- i3-gaps
- urxvt
- polybar
- rofi
- vis
- picom (compton)

*Optional:*
- firefox
- spotify
- code-oss
- virtualbox

**Ubuntu Installation**

**snapd required for VSCode and Spotify installation!**

**!!! NO i3-gaps in official repository!!!**

`sudo apt-get install i3 rxvt-unicode rofi vis compton firefox virtualbox && snap install code && snap install spotify`

To install polybar visit [official polybar repository](https://github.com/polybar/polybar)


**Arch/Manjaro Installation**

`sudo pacman -S i3-gaps rxvt-unicode rofi vis picom firefox virtualbox code`

Install spotify from AUR

`yay -s spotify`

**Running installation script**

`cd dotFiles`

Mark shell script as executable

`chmod +x install.sh`

Run the script

`./install.sh`