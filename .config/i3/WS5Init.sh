i3-msg workspace number 5:
i3-msg for_window [class="URxvt"] floating enable

sleep 0.5
i3-msg "workspace 5:; exec urxvt -e bash -c 'sleep 3 && neofetch && bash'"
sleep 1
i3-msg "workspace 5:; exec urxvt -e bash -c 'htop && bash'"
sleep 1
i3-msg "workspace 5:; exec urxvt -e bash -c 'vis && bash'"
sleep 0.5
sh ~/.config/i3/layout-manager.sh WS5

i3-msg for_window [class="URxvt"] floating disable
