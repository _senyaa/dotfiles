[global/wm]
margin-top = 0
margin-bottom = 0

[settings]
throttle-output = 5
throttle-output-for = 10
throttle-input-for = 30
screenchange-reload = true
compositing-background = over
compositing-foreground = over
compositing-overline = over
compositing-underline = over
compositing-border = over

; Define fallback values used by all module formats
format-foreground = #FF0000
format-background = #fffff
format-underline =
format-overline =
format-spacing =
format-padding =
format-margin =
format-offset =

[colors]
background = #545754
foreground = #7AE582
alert = #bd2c40
volume-min = #a3be8c
volume-med = #ebcb8b
volume-max = #bf616a


################################################################################
################################################################################
############                         MAINBAR-I3                     ############
################################################################################
################################################################################

[bar/mainbar-i3]

monitor = ${env:MONITOR}
;monitor-fallback = HDMI1
monitor-strict = false
override-redirect = false
bottom = false
fixed-center = true
width = 100%
height = 20
;offset-x = 1%
;offset-y = 1%

background = ${colors.background}
foreground = ${colors.foreground}

; Background gradient (vertical steps)
;   background-[0-9]+ = #aarrggbb
;background-0 =

radius = 0.0
line-size = 2
line-color = #000000

border-size = 0
;border-left-size = 25
;border-right-size = 25
;border-top-size = 0
;border-bottom-size = 25
border-color = #000000

padding-left = 1
padding-right = 1

module-margin-left = 0
module-margin-right = 0

font-0 = "UbuntuMono Nerd Font:size=10;2"
font-1 = "UbuntuMono Nerd Font:size=16;3"
font-2 = "Font Awesome 5 Free:style=Regular:pixelsize=10;1"
font-3 = "Font Awesome 5 Free:style=Solid:pixelsize=10;1"
font-4 = "Font Awesome 5 Brands:pixelsize=10;1"

modules-left = i3 xwindow
modules-center = spotify1

modules-right = filesystem arrow3 memory2 arrow3 cpu2 arrow2 date
;modules-right = arrow1 networkspeedup networkspeeddown arrow2 memory2 arrow3 cpu2 arrow2 pavolume arrow3 arch-aur-updates arrow2 date

separator = 

;dim-value = 1.0

tray-position = right
tray-detached = false
tray-maxsize = 20
tray-background = ${colors.background}
tray-offset-x = 0
tray-offset-y = 0
tray-padding = 4
tray-scale = 1.0

#i3: Make the bar appear below windows
;wm-restack = i3
;override-redirect = true

; Enable support for inter-process messaging
; See the Messaging wiki page for more details.
enable-ipc = true

; Fallback click handlers that will be called if
; there's no matching module handler found.
click-left =
click-middle =
click-right =
scroll-up = i3wm-wsnext
scroll-down = i3wm-wsprev
double-click-left =
double-click-middle =
double-click-right =

cursor-click =
cursor-scroll =




################################################################################
################################################################################
############                       MODULE I3                        ############
################################################################################
################################################################################

[module/i3]
;https://github.com/jaagr/polybar/wiki/Module:-i3
type = internal/i3

; Only show workspaces defined on the same output as the bar
;
; Useful if you want to show monitor specific workspaces
; on different bars
;
; Default: false
pin-workspaces = true

; This will split the workspace name on ':'
; Default: false
strip-wsnumbers = false

; Sort the workspaces by index instead of the default
; sorting that groups the workspaces by output
; Default: false
index-sort = false

; Create click handler used to focus workspace
; Default: true
enable-click = true

; Create scroll handlers used to cycle workspaces
; Default: true
enable-scroll = true

; Wrap around when reaching the first/last workspace
; Default: true
wrapping-scroll = false

; Set the scroll cycle direction
; Default: true
reverse-scroll = false

; Use fuzzy (partial) matching on labels when assigning
; icons to workspaces
; Example: code;♚ will apply the icon to all workspaces
; containing 'code' in the label
; Default: false
fuzzy-match = false


; Available tags:
;   <label-state> (default) - gets replaced with <label-(focused|unfocused|visible|urgent)>
;   <label-mode> (default)
format = <label-state> <label-mode>

label-mode = %mode%
label-mode-padding = 2
label-mode-foreground = #000000
label-mode-background = #FFBB00

; Available tokens:
;   %name%
;   %icon%
;   %index%
;   %output%
; Default: %icon%  %name%
; focused = Active workspace on focused monitor
label-focused = %icon% %name%
label-focused-background = ${colors.background}
label-focused-foreground = ${colors.foreground}
label-focused-underline = #9fffcb
label-focused-padding = 2

; Available tokens:
;   %name%
;   %icon%
;   %index%
; Default: %icon%  %name%
; unfocused = Inactive workspace on any monitor
label-unfocused = %icon% %name%
label-unfocused-padding = 2
label-unfocused-background = ${colors.background}
label-unfocused-foreground = ${colors.foreground}
label-unfocused-underline =

; visible = Active workspace on unfocused monitor
label-visible = %icon% %name%
label-visible-background = ${self.label-focused-background}
label-visible-underline = ${self.label-focused-underline}
label-visible-padding = 2

; Available tokens:
;   %name%
;   %icon%
;   %index%
; Default: %icon%  %name%
; urgent = Workspace with urgency hint set
label-urgent = %icon% %name%
label-urgent-background = ${self.label-focused-background}
label-urgent-foreground = #db104e
label-urgent-padding = 2

format-foreground = ${colors.foreground}
format-background = ${colors.background}


################################################################################
###############################################################################
############                       MODULES ARROWS                     ############
################################################################################
################################################################################


[module/arrow1]
; grey to Blue
type = custom/text
content = "%{T2} %{T-}"
content-font = 2
content-foreground = #ffffff
content-background = #3f6634

[module/arrow2]
; grey to Blue
type = custom/text
content = "%{T2} %{T-}"
content-font = 2
content-foreground = #ffffff
content-background = #0a8754

[module/arrow3]
; grey to Blue
type = custom/text
content = "%{T2} %{T-}"
content-font = 2
content-foreground = #ffffff
content-background = #004e64

################################################################################
###############################################################################
############                       MODULES A-Z                      ############
################################################################################
################################################################################

;[module/arch-aur-updates]
;type = custom/script
;exec = ~/.config/polybar/scripts/check-all-updates.sh
;interval = 1000
;label = Updates: %output%
;format-foreground = #fefefe
;format-background = #8d62ad
;format-prefix = "  "
;format-prefix-foreground = #fefefe


;[module/aur-updates]
;type = custom/script
;exec = cower -u | wc -l
;interval = 1000
;label = Aur: %output%
;format-foreground = ${colors.foreground}
;format-background = ${colors.background}
;format-prefix = "  "
;format-prefix-foreground = #FFBB00
;format-underline = #FFBB00

################################################################################

[module/compton]
;https://github.com/jaagr/polybar/wiki/User-contributed-modules#compton
type = custom/script
exec = ~/.config/polybar/scripts/compton.sh
click-left = ~/.config/polybar/scripts/compton-toggle.sh
interval = 5
format-foreground = ${colors.foreground}
format-background = ${colors.background}
;format-underline = #00AF02

################################################################################

[module/cpu2]
;https://github.com/jaagr/polybar/wiki/Module:-cpu
type = internal/cpu
; Seconds to sleep between updates
; Default: 1
interval = 1
format-foreground = #7ae582
format-background = #0a8754
format-prefix = "  "
format-prefix-foreground = #fefefe

label-font = 1

; Available tags:
;   <label> (default)
;   <bar-load>
;   <ramp-load>
;   <ramp-coreload>
format = <label>


; Available tokens:
;   %percentage% (default) - total cpu load
;   %percentage-cores% - load percentage for each core
;   %percentage-core[1-9]% - load percentage for specific core
label = CPU %percentage:3%%

################################################################################

[module/date]
;https://github.com/jaagr/polybar/wiki/Module:-date
type = internal/date
; Seconds to sleep between updates
interval = 5
; See "http://en.cppreference.com/w/cpp/io/manip/put_time" for details on how to format the date string
; NOTE: if you want to use syntax tags here you need to use %%{...}
date = " %a %b %d, %Y"
date-alt = " %a %b %d, %Y"
time = %l:%M%p
time-alt = %l:%M%p
format-prefix = " "
format-prefix-foreground = #fefefe
format-foreground = #7ae582
format-background = #3f6634
label = "%date% %time% "


################################################################################

[module/filesystem]
type = internal/fs


mount-0 = /

; Seconds to sleep between updates
; Default: 30
interval = 30

; Display fixed precision values
; Default: false
fixed-values = false

; Spacing between entries
; Default: 2
spacing = 4

; Available tags:
;   <label-mounted> (default)
;   <bar-free>
;   <bar-used>
;   <ramp-capacity>
format-mounted = <label-mounted>
format-mounted-foreground = ${colors.foreground}
format-mounted-background = ${colors.background}
format-mounted-underline = #00a5cf


; Available tokens:
;   %mountpoint%
;   %type%
;   %fsname%
;   %percentage_free%
;   %percentage_used%
;   %total%
;   %free%
;   %used%
; Default: %mountpoint% %percentage_free%%
label-mounted = %mountpoint% : %used% used of %total%

; Available tokens:
;   %mountpoint%
; Default: %mountpoint% is not mounted
label-unmounted = %mountpoint% not mounted
format-unmounted-foreground = ${colors.foreground}
format-unmounted-background = ${colors.background}
;format-unmounted-underline = ${colors.alert}


################################################################################

[module/load-average]
type = custom/script
exec = uptime | grep -ohe 'load average[s:][: ].*' | awk '{ print $3" "$4" "$5"," }' | sed 's/,//g'
interval = 100

;HOW TO SET IT MINIMAL 10 CHARACTERS - HIDDEN BEHIND SYSTEM ICONS
;label = %output%
label = %output:10%

format-foreground = ${colors.foreground}
format-background = ${colors.background}
format-prefix = "  "
format-prefix-foreground = #62FF00
format-underline = #62FF00

################################################################################

[module/memory2]
;https://github.com/jaagr/polybar/wiki/Module:-memory
type = internal/memory
interval = 1
; Available tokens:
;   %percentage_used% (default)
;   %percentage_free%
;   %gb_used%
;   %gb_free%
;   %gb_total%
;   %mb_used%
;   %mb_free%
;   %mb_total%
label = %percentage_used%%

format = RAM <label>
format-prefix = "  "
format-prefix-foreground = #fefefe
format-foreground = #7ae582
format-background = #004e64

################################################################################

[module/pacman-updates]
type = custom/script
;exec = pacman -Qu | wc -l
exec = checkupdates | wc -l
interval = 1000
label = Repo: %output%
format-foreground = ${colors.foreground}
format-background = ${colors.background}
format-prefix = "  "
format-prefix-foreground = #FFBB00
format-underline = #FFBB00

################################################################################

[module/spotify1]
;https://github.com/NicholasFeldman/dotfiles/blob/master/polybar/.config/polybar/spotify.sh
type = custom/script
exec = ~/.config/polybar/scripts/spotify1.sh
interval = 1

;format = <label>
format-foreground = ${colors.foreground}
format-background = ${colors.background}
format-padding = 2
format-underline = #0f0
format-prefix = "  "
format-prefix-foreground = #0f0
label = %output:0:150%

################################################################################

[module/uptime]
;https://github.com/jaagr/polybar/wiki/User-contributed-modules#uptime
type = custom/script
exec = uptime | awk -F, '{sub(".*up ",x,$1);print $1}'
interval = 100
label = Uptime : %output%

format-foreground = ${colors.foreground}
format-background = ${colors.background}
format-prefix = " "
format-prefix-foreground = #C15D3E
format-underline = #C15D3E

################################################################################

[module/volume]
;https://github.com/jaagr/polybar/wiki/Module:-volume
type = internal/volume
format-volume = "<label-volume>  <bar-volume>"

label-volume = " "
label-volume-foreground = #40ad4b
label-muted = muted

bar-volume-width = 10
bar-volume-foreground-0 = #40ad4b
bar-volume-foreground-1 = #40ad4b
bar-volume-foreground-2 = #40ad4b
bar-volume-foreground-3 = #40ad4b
bar-volume-foreground-4 = #40ad4b
bar-volume-foreground-5 = #40ad4b
bar-volume-foreground-6 = #40ad4b
bar-volume-gradient = false
bar-volume-indicator = 
bar-volume-indicator-font = 2
bar-volume-fill = 
bar-volume-fill-font = 2
bar-volume-empty = 
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.foreground}
format-volume-foreground = ${colors.foreground}
format-volume-background = ${colors.background}
format-muted-prefix = "  "
format-muted-prefix-foreground = "#ff0000"
format-muted-foreground = ${colors.foreground}
format-muted-background = ${colors.background}

################################################################################

[module/weather]
type = custom/script
interval = 10
format = <label>
format-prefix = " "
format-prefix-foreground = #3EC13F
format-underline = #3EC13F
format-foreground = ${colors.foreground}
format-background = ${colors.background}
exec = python -u ~/.config/polybar/scripts/weather.py
tail = true

#################################################################################

[module/wired-network]
;https://github.com/jaagr/polybar/wiki/Module:-network
type = internal/network
interface = enp4s0
;interface = enp14s0
interval = 3.0

; Available tokens:
;   %ifname%    [wireless+wired]
;   %local_ip%  [wireless+wired]
;   %essid%     [wireless]
;   %signal%    [wireless]
;   %upspeed%   [wireless+wired]
;   %downspeed% [wireless+wired]
;   %linkspeed% [wired]
; Default: %ifname% %local_ip%
label-connected =  %ifname%
label-disconnected = %ifname% disconnected

format-connected-foreground = ${colors.foreground}
format-connected-background = ${colors.background}
format-connected-underline = #55aa55
format-connected-prefix = " "
format-connected-prefix-foreground = #55aa55
format-connected-prefix-background = ${colors.background}

format-disconnected = <label-disconnected>
format-disconnected-underline = ${colors.alert}
label-disconnected-foreground = ${colors.foreground}

################################################################################

[module/xbacklight]
;https://github.com/jaagr/polybar/wiki/Module:-xbacklight
type = internal/xbacklight
format = <label> <bar>
format-prefix = "   "
format-prefix-foreground = ${colors.foreground}
format-prefix-background = ${colors.background}
format-prefix-underline = #9f78e1
format-underline = #9f78e1
label = %percentage%%
bar-width = 10
bar-indicator = 
bar-indicator-foreground = #fff
bar-indicator-font = 2
bar-fill = 
bar-fill-font = 2
bar-fill-foreground = #9f78e1
bar-empty = 
bar-empty-font = 2
bar-empty-foreground = #fff
format-foreground = ${colors.foreground}
format-background = ${colors.background}

################################################################################

[module/xkeyboard]
;https://github.com/jaagr/polybar/wiki/Module:-xkeyboard
type = internal/xkeyboard
blacklist-0 = num lock

format-prefix = " "
format-prefix-foreground = ${colors.foreground}
format-prefix-background = ${colors.background}
format-prefix-underline = #3ecfb2
format-foreground = ${colors.foreground}
format-background = ${colors.background}

label-layout = %layout%
label-layout-underline = #3ecfb2
label-indicator-padding = 2
label-indicator-margin = 1
label-indicator-background = ${colors.background}
label-indicator-underline = ${colors.foreground}


################################################################################

[module/xmenu]
type = custom/script
interval = 1200
exec = echo "  "
click-left = "sh /home/dt/xmenu/xmenu.sh"
format-foreground = ${colors.foreground}
format-background = ${colors.background}
################################################################################

[module/xwindow]
;https://github.com/jaagr/polybar/wiki/Module:-xwindow
type = internal/xwindow

; Available tokens:
;   %title%
; Default: %title%
label = %title%
label-maxlen = 50

format-prefix = "  "
format-prefix-underline = #292d3e
format-underline = #9fffcb
format-foreground = #7ae582
format-background = ${colors.background}

###############################################################################
# vim:ft=dosini
